function createNewUser() {
   const firstName = prompt("Введіть ім'я:");
   const lastName = prompt("Введіть прізвище:");
   const birthday = prompt("Введіть дату народження у форматі dd.mm.yyyy:");

   const newUser = {
     firstName: firstName,
     lastName: lastName,
     birthday: birthday,
     getLogin: function () {
       const firstLetter = this.firstName.charAt(0).toLowerCase();
       return firstLetter + this.lastName.toLowerCase();
     },
     getAge: function () {
       const today = new Date();
       const birthDate = new Date(this.birthday.split('.').reverse().join('.'));
       const age = today.getFullYear() - birthDate.getFullYear();
       const monthDiff = today.getMonth() - birthDate.getMonth();
       
       if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
         return age - 1;
       } else {
         return age;
       }
     },
     getPassword: function () {
       const firstLetter = this.firstName.charAt(0).toUpperCase();
       const lastNameLowercase = this.lastName.toLowerCase();
       const birthYear = this.birthday.split('.')[2];
       return firstLetter + lastNameLowercase + birthYear;
     },
   };

   return newUser;
 }

 const user = createNewUser();
 const login = user.getLogin();
 const age = user.getAge();
 const password = user.getPassword();

 console.log("Об'єкт користувача:", user);
 console.log("Логін користувача:", login);
 console.log("Вік користувача:", age);
 console.log("Пароль користувача:", password);

 